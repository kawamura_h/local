# -*- coding: utf-8 -*-
"""
Created on Fri Mar  4 18:28:22 2016

@author: SIP_HU
"""

import sys,os
sys.path.append(os.path.dirname(os.path.abspath(__file__)) + '/../IfcConverter')

from ifcutil import *

import json

import csv

class ifcOperation:
    def __init__(self):
        self._ifc = ifcUtil(ifc());
        self._bcolor = ["ff0000","00ff00","0000ff"];
        self._fcolor = ["ffffff","000000","ffffff"];
        
    def getDegradationInfo(self,stepid):
        t=self._ifc._ifcc.GetInstanceOfId(int(stepid));
        p=self._ifc.GetPropertyLists(t);
        pl=self._ifc.GetProperties(p['bmPset_DegradationInformation']);
        m=self._ifc._ifcc.GetAgrInsAttr(t,"MeasuredBy");
        p=self._ifc._ifcc.GetInsAttr(m[0],"RelatingRegion");
        m=self._ifc.GetDocumentInfo(p);
        pl['ImageFile']=m[1];
        m=self._ifc._ifcc.GetAgrInsAttr(t,"ReferencedBy");
        p=self._ifc._ifcc.GetAgrInsAttr(m[0],"RelatedObjects");
        pl['ImageDate']=self._ifc.GetDateInfoOfTask(p[0]);
        return pl;

    def IfcInit(self,ifcfile):
        print(self._ifc._ifcc.UnicodeOpen(ifcfile,"..\Data\IFC4_ADD1_BMaintenance.exp"));

    def getMeasurementInfo(self):
        retval = {"mDate":[],"dTag":[],"dColor":[]}
        t=self._ifc._ifcc.FindInstanceBN("IFCTASK");
        k=0;
        for x in t:
            if(self._ifc._ifcc.GetStrAttr(x,"name")=="Inspection Task" ):
                retval["mDate"].append(self._ifc.GetDateInfoOfTask(x));
                retval["dColor"].append([self._bcolor[k],self._fcolor[k]]);
                dt=self._ifc._ifcc.GetAgrInsAttr(x,"HasAssignments");
                listd=[];
                for y in dt:
                    degel={};
                    z=self._ifc._ifcc.GetInsAttr(y,"RelatingProduct");
                    if self._ifc._ifcc.InstanceClass(z)=="IfcDegradationElement" :
                        degel["stepid"]=self._ifc._ifcc.GetInstanceId(z);
                        z=self._ifc._ifcc.GetInsAttr(z,"ObjectPlacement");
                        z=self._ifc._ifcc.GetInsAttr(z,"RelativePlacement");
                        z=self._ifc._ifcc.GetInsAttr(z,"Location");
                        degel["Coord"]=self._ifc._ifcc.GetAgrRealAttr(z,"Coordinates");
                        listd.append(degel);
                retval["dTag"].append(listd);
                k=k+1;
        return(retval);
        
    def SetMeasurementInfo(self,imfile,iffile,degtype,deggrade):
        pset={"bmPset_DegradationInformation":{'bm_DegradationType':degtype, 'bm_DegradationGrade':deggrade}};
        f = open("image/"+iffile, 'r');
        reader = csv.reader(f);
        row =next(reader);
        p=[];
        for i in range(0,11):
            p.append(float(row[i]));

        tsk=self._ifc.CreateTask(row[11]);#Task
        deg=self._ifc._ifcc.CreateInstance("IfcDegradation");#Degradation
        self._ifc._ifcc.PutStrAttr(deg,"Name",degtype);
        tmp=self._ifc._ifcc.CreateInstance("IfcRelConnectsToTimeVariations");
        self._ifc._ifcc.PutInsAttr(tmp,"RelatingProduct",deg);
        dege=self._ifc._ifcc.CreateInstance("IfcDegradationElement");#DegradationElement
        self._ifc._ifcc.PutAgrInsAttr(tmp,"RelatedProducts",[dege]);
        tmp=self._ifc._ifcc.CreateInstance("IfcRelAssignsToProduct");
        self._ifc._ifcc.PutInsAttr(tmp,"RelatingProduct",dege);
        self._ifc._ifcc.PutAgrInsAttr(tmp,"RelatedObjects",[tsk]);
        tmp=self._ifc._ifcc.CreateInstance("IfcLocalPlacement");
        tmp2=self._ifc.SetAxis2Placement3d([[p[0],p[1],p[2]]]);
        self._ifc._ifcc.PutInsAttr(tmp,"RelativePlacement",tmp2);
        self._ifc._ifcc.PutInsAttr(dege,"ObjectPlacement",tmp);
        self._ifc.SetPropertyList(dege,pset);
        mr=self._ifc._ifcc.CreateInstance("IfcMeasuredRegion");#MeasuredRegion
        self._ifc._ifcc.PutStrAttr(mr,"Name","degradation image");
        tmp=self._ifc._ifcc.CreateInstance("IfcRelAssignsToProduct");
        self._ifc._ifcc.PutInsAttr(tmp,"RelatingProduct",mr);
        self._ifc._ifcc.PutAgrInsAttr(tmp,"RelatedObjects",[tsk]);
        tmp2=self._ifc._ifcc.CreateInstance("IfcDocumentInformation");
        self._ifc._ifcc.PutStrAttr(tmp2,"Identification","");
        self._ifc._ifcc.PutStrAttr(tmp2,"Name","");
        self._ifc._ifcc.PutStrAttr(tmp2,"Location",imfile);
        self._ifc._ifcc.PutStrAttr(tmp2,"ElectronicFormat","image/jpeg");
        tmp=self._ifc._ifcc.CreateInstance("IfcRelAssociatesDocument");
        self._ifc._ifcc.PutInsAttr(tmp,"RelatingDocument",tmp2);
        self._ifc._ifcc.PutAgrInsAttr(tmp,"RelatedObjects",[mr]);
        tmp=self._ifc._ifcc.CreateInstance("IfcLocalPlacement");
        tmp2=self._ifc.SetAxis2Placement3d([[p[0],p[1],p[2]],[p[3],p[4],p[5]],[p[6],p[7],p[8]]]);
        self._ifc._ifcc.PutInsAttr(tmp,"RelativePlacement",tmp2);
        self._ifc._ifcc.PutInsAttr(mr,"ObjectPlacement",tmp);
        tmp=self._ifc._ifcc.CreateInstance("IfcRelConnectsToMeasuredRegion");
        self._ifc._ifcc.PutInsAttr(tmp,"RelatingRegion",mr);
        self._ifc._ifcc.PutAgrInsAttr(tmp,"RelatedProducts",[dege]);
  
        f.close();        
        #self._ifc._ifcc.UnicodeSave("E:\\201603ver\\IfcData\\WakamiyaDegradation.ifc");
        #self.IfcInit("..\..\IfcData\WakamiyaDegradation.ifc");
        #self._ifc._ifcc.UnicodeSave("C:\\Users\\SIP_HU\\Desktop\\Out.ifc");
        return(True);
        #self._ifc._ifcc.UnicodeSave("test.ifc");


if __name__ == '__main__':
    ifc=ifcOperation();
    ifc.IfcInit("..\Data\WakamiyaDegradation.ifc");
    ifc.SetMeasurementInfo("degradation1.jpg","degradation1.csv","Crack","b");
    #json_data = ifc.getDegradationInfo('268278');
    #json_data = ifc.getMeasurementInfo();
    #print(json_data);
    #print(type(json_data))
    #encode_json_data = json.dumps(json_data)
    #print(encode_json_data)
    #print(type(encode_json_data))
