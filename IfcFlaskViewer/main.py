# -*- coding: utf-8 -*-
"""
Created on Sun Jan 17 14:56:38 2016

@author: Fumiki
http://127.0.0.1:5000/

"""

from flask import render_template,Flask,send_from_directory
from flask import jsonify,request
import json

from IfcOperation import *


app = Flask(__name__)

ifc=ifcOperation();

#Initioal (root) Operation
@app.route('/')
def ViewerMain():
    global ifc;
    ifc.IfcInit("..\Data\WakamiyaDegradation.ifc");
#    return render_template('W_Brige.html')
    return render_template('M_Brige.html')

#obj directory
@app.route('/obj/<filename>')
def load_obj_file(filename):
    return send_from_directory('obj',filename)

#image directory
@app.route('/image/<filename>')
def load_img_file(filename):
    return send_from_directory('image',filename)

#maker Information sending
@app.route('/marker', methods=['GET','POST'])
def markerInfomation():
    global ifc;
    info = ifc.getMeasurementInfo();
    print(info);
    return jsonify(ResultSet=json.dumps(info));

#Tagfile operation Dynamic 
@app.route('/taghtml/<name>')
def degradateinfo(name=None):
    global ifc;
    tclass = ifc.getDegradationInfo(name)
    return render_template('degradation.html', name=name,tclass=tclass)

#
@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('input.html', name=name)
    
@app.route('/upload', methods=['POST'])
def upload():
    global ifc;
    the_file1 = request.files['info_file'];
    the_file1.save("./image/" + the_file1.filename);
    the_file2 = request.files['img_file'];
    the_file2.save("./image/" + the_file2.filename);
    print (request.form["Dname"]+":"+request.form["Dgard"] );   # 999
    tst=ifc.SetMeasurementInfo(the_file2.filename,the_file1.filename,request.form["Dname"],request.form["Dgard"])
    if tst:
        return "OK"


if __name__ == '__main__':
    app.run()
 #   app.run(debug=True)

