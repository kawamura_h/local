function CallOBJ() {
	var onProgress = function ( xhr ) {};
	var onError = function ( xhr ) {};
	var loader = new THREE.OBJMTLLoader();
	loader.load( 'obj/wakamiya3.obj', 'obj/wakamiya.mtl', function ( obj ) {
		scene.add( obj );
		objdata = obj;
	}, onProgress, onError );
}

var mobjects = [[],[]];

function Markerset(x, y, z, namev,col,id){
	var geometry = new THREE.SphereGeometry(300, 8, 6, 0, 2*Math.PI, 0, Math.PI);
	var object = new THREE.Mesh( geometry, new THREE.MeshPhongMaterial( { color: col , wireframe:false, wireframeLinewidth:5.0 } ) );
	object.name = namev;
	object.position.set(x, y, z);
	scene.add(object);
	objects.push( object );
	mobjects[id].push(object);
}

function adddata() {
	window.open("hello/input.html", "", "width=500,height=400");
}
function asisswitch() {
	if(objdata.visible){
		objdata.visible = false;
	}else{
		objdata.visible = true;
	}
}
function vis(i) {
	var vflag2;
	if(mobjects[i][0].visible){
		vflag2 = false;
	}else{
		vflag2 = true;
	}
	for(j=0; j < mobjects[i].length;j++){
		mobjects[i][j].visible = vflag2;
	}
}
function Buttonset(top, bname, act,col,bgcol){
	var info = document.createElement( 'div' );
	info.style.position = 'absolute';
	info.style.top = top;
	info.style.width = '100%';
	info.style.textAlign = 'left';
    info.setAttribute("id","i"+bname);
	info.innerHTML = '<input type="button" style="color:#'+col+';background-color:#'+bgcol+'" value="'+bname+'" onClick="'+act+'" />';
	container.appendChild( info );
}

function CallButton(){
	Buttonset('60px','  revise ','adddata()','000000','ffffff');
	Buttonset('90px','as-built','asisswitch()','000000','ffffff');
	//Buttonset('120px','size-down','asisswitch()','000000','ffffff');
	
/*
	Buttonset('70px','2011-10-12','vis(0)','ffffff','ff0000');
    var childs = container.childNodes; 
    alert("test="+childs.length+":"+(childs[2].childNodes[0].value));
*/
}

function CallMarker(){
	//Markerset(30200.0,-1000.0,-6500.0,'268278',0xff0000,0);
	Markerset(20000.0,2000.0,-1680.0,'268350',0xff0000,0);
    markerSet();
}
