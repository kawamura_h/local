from flask import jsonify

@app.route('/interactive/')
def interactive():
	return render_template('interactive.html')
#...other code...

@app.route('/background_process')
def background_process():
	try:
		lang = request.args.get('proglang', 0, type=str)
		if lang.lower() == 'python':
			return jsonify(result='You are wise')
		else:
			return jsonify(result='Try again.')
	except Exception as e:
		return str(e)