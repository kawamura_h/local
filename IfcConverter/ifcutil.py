# -*- coding: utf-8 -*-
"""
Utility for IfcEngine Interface 
Created on Thu Feb 11 16:15:09 2016

@author: Fumiki Tanaka
"""
from ifc import *

class ifcUtil:
    """ IfcEngine Interface. """

    def __init__(self,ifcCore):
        self._ifcc  = ifcCore;
        self._ifc = ifcCore._ifc;
         #constant for ifcEngine.dll
        _flagbit2 = 4;
        _flagbit3 = 8;
        _flagbit5 = 32;
        _flagbit8 = 256;
        _flagbit12 = 4096;
        self._mask = _flagbit2+_flagbit3+_flagbit5+_flagbit8+_flagbit12;
        _setting = 0;
        _setting += _flagbit2;     #    PRECISION (32/64 bit)
    #    _setting += _flagbit3;     #    INDEX ARRAY (32/64 bit)
    #    _setting += _flagbit5;     #    NORMALS ON
        _setting += _flagbit8;     #    TRIANGLES ON
    #    _setting += _flagbit12;	 #    WIREFRAME ON
        self._setting = _setting;
       
    def GetPropertyLists(self,ins):
        """ Obtain all Property Lists of instance. """
        dtagg=c_int();
        dt=c_int();
        p=c_wchar_p();
        retval={};
        self._ifc.sdaiGetAttrBN(ins,"IsDefinedBy",self._ifcc._sdaiAGGR,byref(dtagg));
        for i in list(range(self._ifc.sdaiGetMemberCount(dtagg))):
            self._ifc.engiGetAggrElement(dtagg,i,self._ifcc._sdaiINSTANCE,byref(dt));
            self._ifc.sdaiGetAttrBN(dt,"RelatingPropertyDefinition",self._ifcc._sdaiINSTANCE,byref(dt));
            self._ifc.sdaiGetAttrBN(dt,"name",self._ifcc._sdaiUNICODE, byref(p));
            retval[p.value]=dt.value;
        return(retval);
       
    def GetProperties(self,ins):
        """ Obtain all Properties of PropertyList instance. """
        dtagg=c_int();
        dt=c_int();
        pn=c_wchar_p();
        p=c_wchar_p();
        retval={};
        self._ifc.sdaiGetAttrBN(ins,"HasProperties",self._ifcc._sdaiAGGR,byref(dtagg));
        for i in list(range(self._ifc.sdaiGetMemberCount(dtagg))):
            self._ifc.engiGetAggrElement(dtagg,i,self._ifcc._sdaiINSTANCE,byref(dt));
            self._ifc.sdaiGetAttrBN(dt,"Name",self._ifcc._sdaiUNICODE, byref(pn));
            self._ifc.sdaiGetAttrBN(dt,"NominalValue",self._ifcc._sdaiADB,byref(dt));
            self._ifc.sdaiGetADBValue(dt,self._ifcc._sdaiUNICODE, byref(p));
            retval[pn.value]=p.value;
        return(retval);

    def GetDocumentInfo(self,ins):
        """ Obtain all Document Information of Document """
        dtagg=c_int();
        dt=c_int();
        p=c_wchar_p();
        retval=[];
        self._ifc.sdaiGetAttrBN(ins,"HasAssociations",self._ifcc._sdaiAGGR,byref(dtagg));
        for i in list(range(self._ifc.sdaiGetMemberCount(dtagg))):
            self._ifc.engiGetAggrElement(dtagg,i,self._ifcc._sdaiINSTANCE,byref(dt));
            self._ifc.sdaiGetAttrBN(dt,"RelatingDocument",self._ifcc._sdaiINSTANCE,byref(dt));
            self._ifc.sdaiGetAttrBN(dt,"ElectronicFormat",self._ifcc._sdaiUNICODE, byref(p));
            retval.append(p.value);            
            self._ifc.sdaiGetAttrBN(dt,"Location",self._ifcc._sdaiUNICODE, byref(p));
            retval.append(p.value);
        return(retval)

    def GetDateInfoOfTask(self,ins):
        """ Obtain Finish date Information of Task """
        m=self._ifcc.GetInsAttr(ins,"TaskTime");
        s=self._ifcc.GetStrAttr(m,"ActualFinish");
        st=s.split("T");
        return(st[0]);
    
    def GetSpatialStructure(self):
         """ Obtain Spatial Structure """
         retval=self._ifcc.FindInstanceBN("IfcProject");
         idx = 0;
         while idx < len(retval):
            p=self._ifcc.GetAgrInsAttr(retval[idx],"IsDecomposedBy");
            for ins in p:
                 retval=retval+self._ifcc.GetAgrInsAttr(ins,"RelatedObjects");
            idx += 1;
         return(retval)

    def GetPhysicalStructure(self,sp):
         """ Obtain Physical Structure of Spatial Structure"""
         retval=[];
         p=self._ifcc.GetAgrInsAttr(sp,"ContainsElements");
         for ins in p:
            retval=retval+self._ifcc.GetAgrInsAttr(ins,"RelatedElements");
         return(retval)    

    def GetTriangle(self,ins,func):
        """ Obtain Triangle Data of instance """
#        ins=self._ifcc.GetInsAttr(ins,"Representation");
#        ains=self._ifcc.GetAgrInsAttr(ins,"Representations");
#        ains=self._ifcc.GetAgrInsAttr(ains[0],"Items");
#        ins = ains[0];
        noVertices=c_int();
        noIndices=c_int();
        self._ifc.setFormat(self._ifcc._model, self._setting, self._mask);
        self._ifc.initializeModellingInstance(self._ifcc._model,byref(noVertices), byref(noIndices), c_double(0.0), ins);
        print("val={}".format(noVertices.value));
        if(noVertices.value > 0):
            pVType = c_double*(noVertices.value*3); # if NORMALS ON Then *6
            idxType =c_int*(noIndices.value);
            pV = pVType();
            idx = idxType();
            self._ifc.finalizeModelling(self._ifcc._model, byref(pV), byref(idx), 0);
            func([noVertices.value,noIndices.value],pV,idx);
        return ([noVertices.value,noIndices.value]);

    def GetColor(self,ins):
        tmp=self._ifcc.GetInsAttr(ins,"Representation");
        tmp=self._ifcc.GetAgrInsAttr(tmp,"Representations");
        tmp=self._ifcc.GetAgrInsAttr(tmp[0],"Items");
        tmp=self._ifcc.GetInsAttr(tmp[0],"StyledByItem");
        if (tmp == 0):
            return([0.8,0.8,0.8]);
        tmp=self._ifcc.GetAgrInsAttr(tmp,"Styles");
        tmp=self._ifcc.GetAgrInsAttr(tmp[0],"Styles");
        tmp=self._ifcc.GetAgrInsAttr(tmp[0],"Styles");
        tmp=self._ifcc.GetInsAttr(tmp[0],"SurfaceColour");
        if (self._ifcc.InstanceClass(tmp) == "IfcColourRgb") :
            return([self._ifcc.GetRealAttr(tmp,"Red"),
                    self._ifcc.GetRealAttr(tmp,"Green"),
                    self._ifcc.GetRealAttr(tmp,"Blue")]);
        return([0.8,0.8,0.8]);
   
    def CheckRepresentation(self,ins):
        tmp=self._ifcc.GetInsAttr(ins,"Representation");
        if tmp == 0:
            return False
        return True
    
    def CreateTask(self,ttime):
        tsklist=self._ifcc.FindInstanceBN("IfcTask");
        for tsk in tsklist:
            if(self.GetDateInfoOfTask(tsk) == ttime.split("T")[0]):
                return(tsk);
        ins=self._ifcc.CreateGuidInstance("IfcTask");
        self._ifcc.PutBooleanAttr(ins,"IsMilestone",False);
        self._ifcc.PutStrAttr(ins,"name","Inspection Task");
        ins2=self._ifcc.CreateInstance("IfcTaskTime");
        self._ifcc.PutStrAttr(ins2,"ActualFinish",ttime);
        self._ifcc.PutInsAttr(ins,"TaskTime",ins2);
        return(ins);
        
    def SetPropertyList(self,ins,plist):
        for psname in plist.keys():
          psins = self._ifcc.CreateGuidInstance("IfcPropertySet");
          self._ifcc.PutStrAttr(psins,"Name",psname);
          pp=[];
          for pname in plist[psname].keys():
                pins = self._ifcc.CreateGuidInstance("IfcPropertySingleValue");
                pp.append(pins);
                self._ifcc.PutStrAttr(pins,"Name",pname);
                self._ifcc.PutADBAttribute(pins,"NominalValue","IFCLABEL",plist[psname][pname]);
          self._ifcc.PutAgrInsAttr(psins,"HasProperties",pp);
          tmp=self._ifcc.CreateGuidInstance("IfcRelDefinesByProperties");
          self._ifcc.PutInsAttr(tmp,"RelatingPropertyDefinition",psins);
          self._ifcc.PutAgrInsAttr(tmp,"RelatedObjects",[ins]);
       
        
        
    def SetAxis2Placement3d(self,clist):
        aval=["Axis","RefDirection"];
        retval = self._ifcc.CreateInstance("IfcAxis2Placement3D");
        ins=self._ifcc.CreateInstance("IfcCartesianPoint");
        self._ifcc.PutAgrRealAttr(ins,"Coordinates",clist[0]);
        self._ifcc.PutInsAttr(retval,"Location",ins);
        for i in range(1,len(clist)):
            ins=self._ifcc.CreateInstance("IfcDirection");
            self._ifcc.PutAgrRealAttr(ins,"DirectionRatios",clist[i]);
            self._ifcc.PutInsAttr(retval,aval[i-1],ins);
        return(retval);
   
if __name__ == '__main__':
    _ifcu= ifcUtil(ifc());
    print(_ifcu._ifcc.UnicodeOpen("..\Data\WakamiyaBridge.ifc","..\Data\IFC4_ADD1_BMaintenance.exp"));
    tsk=_ifcu.CreateTask("2011-10-12T17:00");
    _ifcu.SetPropertyList(tsk,{"bmPset_DegradationInformation":{'bm_DegradationType':'Crack', 'bm_DegradationGrade':'b'}});
    _ifcu.SetAxis2Placement3d([[0.0,0.0,0.0],[1.0,0.0,0.0],[2.0,0.0,0.0]])
    _ifcu._ifcc.UnicodeSave("..\Data\test.ifc");
    _ifcu._ifcc.CloseModel();

""" For Debug XML Structure
#    print(_ifcu._ifcc.UnicodeOpen("..\..\IfcData\Mukawa_degra.ifc","..\..\IfcExpress\IFC4_ADD1_BMaintenance.exp"));
#    print(_ifcu._ifcc.UnicodeOpen("..\..\IfcData\Wakamiya2.ifc","..\..\IfcExpress\IFC4_ADD1_BMaintenance.exp"));
#    print(_ifcu._ifcc.UnicodeOpen("..\..\IfcData\WakamiyaBridge.ifcxml","..\..\IfcExpress\IFC2X3_TC1.exp"));
    print(_ifcu._ifcc.UnicodeOpen("..\..\IfcData\WakamiyaDegradation.ifcxml","..\..\IfcExpress\IFC4_ADD1_BMaintenance.exp"));
    #print(_ifcu._ifcc.UnicodeOpen("test.ifc","..\..\IfcExpress\IFC4_ADD1_BMaintenance.exp"));
    ss=_ifcu.GetSpatialStructure();
    print(len(ss));
    for ins in ss:
        _ifcu._ifcc.CheckInstance(ins);
        ps=_ifcu.GetPhysicalStructure(ins);
        print(len(ps));
        for pins in ps:
            _ifcu._ifcc.CheckInstance(pins);
            _ifcu.GetTriangle(pins,tmp);
"""            