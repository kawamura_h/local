# -*- coding: utf-8 -*-
"""
ifc.py

IfcEngine Interface 
Created on Thu Feb 11 15:29:33 2016

@author: Fumiki Tanaka
"""
from ctypes import *

import uuid
import base64

class ifc:
    """ IfcEngine Interface. """
    _sdaiADB = 1;
    _sdaiAGGR = _sdaiADB + 1;
    _sdaiBINARY = _sdaiAGGR + 1;
    _sdaiBOOLEAN = _sdaiBINARY + 1;
    _sdaiENUM = _sdaiBOOLEAN + 1;
    _sdaiINSTANCE = _sdaiENUM + 1;
    _sdaiINTEGER = _sdaiINSTANCE + 1;
    _sdaiLOGICAL = _sdaiINTEGER + 1;
    _sdaiREAL = _sdaiLOGICAL + 1;
    _sdaiSTRING = _sdaiREAL + 1;
    _sdaiUNICODE = _sdaiSTRING + 1;
    _sdaiEXPRESSSTRING = _sdaiUNICODE + 1;
    _engiGLOBALID = _sdaiEXPRESSSTRING + 1;
    _IFC='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_$';
    _STD='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';

    def __init__(self):
        self._ifc = windll.LoadLibrary("..\Data\ifcengine32.dll");
        self._model=0;

    def UnicodeOpen(self,fname,sname):
        """ Open ifcfile with schemafile using UNICODE. """
        self._ifc.setStringUnicode(1);
#        self._model = self._ifc.sdaiOpenModelBNUnicode(0,fname,sname);
        self._model = self._ifc.sdaiOpenModelBNUnicode(0,fname,sname);
        p=c_wchar_p();
        self._ifc.GetSPFFHeaderItem(self._model, 9, 0, self._sdaiUNICODE, byref(p));
        return(p.value);

    def UnicodeSave(self,fname):
        """ Open ifcfile with schemafile using UNICODE. """
        self._model = self._ifc.sdaiSaveModelBNUnicode(self._model,fname);

    def UnicodeXMLSave(self,fname):
        """ Open ifcfile with schemafile using UNICODE. """
        self._model = self._ifc.sdaiSaveModelAsXmlBNUnicode(self._model,fname);
        #self._model = self._ifc.sdaiSaveModelAsXmlBN(self._model,fname);

    def CheckInstance(self,ins):
        """ Print instance ID and entity name. """
        print("#", end="")
        print(self.GetInstanceId(ins));
        print(self.InstanceClass(ins));
        
    def GetInstanceId(self,ins):
        """ Obtain Entity name of instance. """
        #print(self._ifc.engiGetInstanceLocalId(ins));
        return(self._ifc.internalGetP21Line(ins));
        
    def GetInstanceOfId(self,ins):
        """ Obtain Entity name of instance. """
        #print(self._ifc.engiGetInstanceLocalId(ins));
        return(self._ifc.internalGetInstanceFromP21Line(self._model,ins));

    def InstanceClass(self,ins):
        """ Obtain Entity name of instance. """
        lid=c_char_p();
        self._ifc.engiGetInstanceClassInfoEx(ins,byref(lid));
        return(lid.value.decode('utf-8'));

    def CloseModel(self):
        """ Close Model """
        self._ifc.sdaiCloseModel(self._model);

    def FindInstanceBN(self,ename):
        """ Find all instances of entity """
        dt=c_int();
        retval=[];
        degagg = self._ifc.sdaiGetEntityExtentBN(self._model, ename);
        for i in list(range(self._ifc.sdaiGetMemberCount(degagg))):
            self._ifc.engiGetAggrElement(degagg,i,self._sdaiINSTANCE,byref(dt));
#            self.CheckInstance(dt);
            retval.append(dt.value);
        return(retval);
        
    def GetInsAttr(self,ins,attr):
        """ Obtain instance typed attribute. """
        p=c_int();
        self._ifc.sdaiGetAttrBN(ins,attr,self._sdaiINSTANCE, byref(p))
        return(p.value)
        
    def GetStrAttr(self,ins,attr):
        """ Obtain string typed attribute. """
        p=c_wchar_p();
        self._ifc.sdaiGetAttrBN(ins,attr,self._sdaiUNICODE, byref(p))
        return(p.value) 

    def GetRealAttr(self,ins,attr):
        """ Obtain real typed attribute. """
        dt=c_double();
        self._ifc.sdaiGetAttrBN(ins,attr,self._sdaiREAL,byref(dt));
        return(dt.value);

    def GetAgrInsAttr(self,ins,attr):
        """ Obtain instance aggregate typed attribute. """
        p=c_int();
        dt=c_int();
        retval=[];
        self._ifc.sdaiGetAttrBN(ins,attr,self._sdaiAGGR, byref(p))
        for i in list(range(self._ifc.sdaiGetMemberCount(p))):
            self._ifc.engiGetAggrElement(p,i,self._sdaiINSTANCE,byref(dt));
#            self.CheckInstance(dt);
            retval.append(dt.value);
        return(retval);

    def GetAgrRealAttr(self,ins,attr):
        """ Obtain real aggregate typed attribute. """
        p=c_int();
        dt=c_double();
        retval=[];
        self._ifc.sdaiGetAttrBN(ins,attr,self._sdaiAGGR, byref(p))
        for i in list(range(self._ifc.sdaiGetMemberCount(p))):
            self._ifc.engiGetAggrElement(p,i,self._sdaiREAL,byref(dt));
            retval.append(dt.value);
        return(retval);
        
    def CreateInstance(self,insname):
        return (self._ifc.sdaiCreateInstanceBN(self._model,insname));
        
    def CreateGuidInstance(self,insname):
        ins=self._ifc.sdaiCreateInstanceBN(self._model,insname);
        self.PutGuid(ins);
        return (ins);

    def PutStrAttr(self,ins,attr,val):
        """ Put string typed attribute. """
        self._ifc.sdaiPutAttrBN(ins,attr,self._sdaiUNICODE, val)
        
    def PutInsAttr(self,ins,attr,val):
        """ Put instance typed attribute. """
        self._ifc.sdaiPutAttrBN(ins,attr,self._sdaiINSTANCE, val)

    def PutAgrInsAttr(self,ins,attr,vals):
        """ Put instance aggregate typed attribute. """
        aggr=self._ifc.sdaiCreateAggrBN(ins,attr);
        for ins in vals:
            self._ifc.sdaiAppend(aggr,self._sdaiINSTANCE,ins);
            
    def PutAgrRealAttr(self,ins,attr,vals):
        """ Put real aggregate typed attribute. """
        aggr=self._ifc.sdaiCreateAggrBN(ins,attr);
        for ins in vals:
            dt=c_double(ins);
            self._ifc.sdaiAppend(aggr,self._sdaiREAL,byref(dt));

    def PutBooleanAttr(self,ins,attr,val):
        """ Put Boolean typed attribute. """
        if val:
            self._ifc.sdaiPutAttrBN(ins,attr,self._sdaiBOOLEAN, "T");
        else:
             self._ifc.sdaiPutAttrBN(ins,attr,self._sdaiBOOLEAN, "");
    
    def PutADBAttribute(self,ins,attr,Label,val):
        self._ifc.sdaiCreateADB.restype = c_void_p;
        vADB = self._ifc.sdaiCreateADB(self._sdaiUNICODE, val);
        self._ifc.sdaiPutADBTypePath(vADB, 1, Label);
        self._ifc.sdaiPutAttrBN(ins,attr,self._sdaiADB, vADB);

    def PutGuid(self,ins):
        guid=uuid.uuid1(); # also uuid4
#        print(guid);
        valstd=base64.b64encode(guid.bytes);
        valifc=valstd.decode('utf-8').translate(str.maketrans(self._STD, self._IFC)).rstrip('==');
        self.PutStrAttr(ins,"GlobalId",valifc)
        return(valifc)

    def decodeGuid(self,valifc):
        print(valifc)
        valifc=valifc+'==';
        valstd=valifc.translate(str.maketrans(self._IFC, self._STD)).encode('utf-8')
        print(uuid.UUID(bytes=base64.b64decode(valstd)))

if __name__ == '__main__':
    _ifc= ifc();
    print(_ifc.UnicodeOpen("..\Data\WakamiyaBridge.ifc","..\Data\IFC4_ADD1_BMaintenance.exp"));
    ins=_ifc.CreateInstance("IfcTask");
    _ifc.PutBooleanAttr(ins,"IsMilestone",True);
    ins2=_ifc.CreateInstance("IfcRelAggregates");
    _ifc.PutAgrInsAttr(ins2,"RelatedObjects",[ins,ins]);
    ins2=_ifc.CreateInstance("IfcCartesianPoint");
    _ifc.PutAgrRealAttr(ins2,"Coordinates",[1.0,0.0,2.0]);
    pins = _ifc.CreateInstance("IfcPropertySingleValue");
    _ifc.PutADBAttribute(pins,"NominalValue","IFCLABEL","b")
    _ifc.UnicodeSave("test.ifc");
    _ifc.UnicodeXMLSave("test.xml");
    
    #_ifc.UnicodeSave("test.ifc");
    #print(dt);
    #_ifc.UnicodeXMLSave("test.ifcxml");
    _ifc.CloseModel();