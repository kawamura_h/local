#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <math.h>
#include <iomanip>


using namespace std;
double const ow = 0.1;
double const ow_left = 1-ow;
double const ow_right = 1+ow;

void vtConverter(ofstream &fout, vector<double> u, vector<double> v, double div, int input) {
	//テクスチャ座標値の修正(1)
	for (int i = 0; i<int(u.size()); i++) {
		int u0 = u[i]*input;
		int v0 = v[i]*input;
		double u1,v1;
		u1 = ((u[i] - div * double(u0)*ow_left) * (input / ow_right));
		if (u1 < 0)u1 = 0;
		if (u1 > 1)u1 = 1;
		v1 = ((v[i] - div * double(v0)*ow_left) * (input / ow_right));
		if (v1 < 0)v1 = 0;
		if (v1 > 1)v1 = 1;
		fout << endl << "vt " << u1 << " " << v1 ;
	}
}
void vtConverter2(ofstream &fout, vector<double> u, vector<double> v, double div, int input, vector<int> vt_area) {
	//テクスチャ座標値の修正(2)
	for (int i = 0; i<int(u.size()); ++i) {

		int u0 = vt_area[i] % input;
		int v0 = vt_area[i] / input;
		double u1, v1;
		u1 = ((u[i] - div * double(u0)*ow_left) * (input / ow_right));
		if (u1 < 0)u1 = 0;
		if (u1 > 1)u1 = 1;
		v1 = ((v[i] - div * double(v0)*ow_left) * (input / ow_right));
		if (v1 < 0)v1 = 0;
		if (v1 > 1)v1 = 1;
		fout << endl << "vt " << u1 << " " << v1;
	}
}
void faceConverter(ofstream &fout, vector<vector<string>>*face, string mtl_name, int input) {
	string str;
	for (int i = 0; i < input*input; ++i) {
		fout << endl << "usemtl " << to_string(i) << "_" << mtl_name;
		for (int j = 0; j < int(face[i].size()); ++j) {
			fout << endl << "f";
			fout << " " << face[i][j][0];
			fout << " " << face[i][j][1];
			fout << " " << face[i][j][2];
		}
	}
}
void faceClassification(ofstream &fout, ifstream &fin, vector<double> u, vector<double> v,vector<double>&u_fix,vector<double>&v_fix, vector<vector<string>>*face, int input,vector<int> &vt_area,double div,int &ref_count) {
	bool boundary = false;
	int vt_start, vt_end;
	int n=0;
	int vt_count = 0;
	int f_count = 0;
	int *area = new int[input*input];
	for (int i = 0; i < input*input; ++i) {
		area[i] = 0;
	}
	double u_buffer[3];
	double v_buffer[3];
	string str;
	string face_element1;
	string face_element2;
	string vt_ref;
	vector<string> face_buffer;

	//faceの情報の読み込み
	fin >> str;
	face_buffer.push_back(str);
	fin >> str;
	face_buffer.push_back(str);
	fin >> str;
	face_buffer.push_back(str);

	//faceが参照するテクスチャ座標ベクトルの特定
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < int(face_buffer[i].size()); ++j) {
			if (vt_count == 0)
				face_element1.push_back(face_buffer[i][j]);
			if (face_buffer[i][j] == '/' && vt_count == 1) {
				vt_end = j;
				++vt_count;
			}
			if (face_buffer[i][j] == '/' && vt_count == 0) {
				vt_start = j;
				++vt_count;
			}
			if (vt_count == 2)
				face_element2.push_back(face_buffer[i][j]);
		}
		vt_count = 0;
		for (int j = vt_start + 1; j < vt_end; ++j) {
			vt_ref.push_back(face_buffer[i][j]);
		}
		u_buffer[i] = u[stoi(vt_ref) - 1];
		v_buffer[i] = v[stoi(vt_ref) - 1];
		//if ((fmod(u_buffer[i], div) < ow*div && fmod(u_buffer[i], div) >= 1-(ow*div))|| (fmod(v_buffer[i], div) < ow*div && fmod(v_buffer[i], div) >= 1 - (ow*div))){
		if((u_buffer[i]>0.45&&u_buffer[i]<0.55)||(v_buffer[i]>0.45&&v_buffer[i]<0.55)){
			boundary = true;
		}
		if (boundary) {
			u_fix.push_back(u_buffer[i]);
			v_fix.push_back(v_buffer[i]);
			++ref_count;
			face_buffer[i] = face_element1 + to_string(u.size() + ref_count) + face_element2;
			++n;
		}
		boundary = false;
		string().swap(vt_ref);
		string().swap(face_element1);
		string().swap(face_element2);
	}
	
	//vt座標値によるfaceの割り当て
	for (int i = 0; i < 3; i++) {
		int u1 = (u_buffer[i] - ow*div)*input;
		if (u1 < 0)u1 = 0;
		int u2 = (u_buffer[i] + ow*div)*input;
		if (u2 == input)u2 = input - 1;
		int v1 = (v_buffer[i] - ow*div)*input;
		if (v1 < 0)v1 = 0;
		int v2 = (v_buffer[i] + ow*div)*input;
		if (v2 == input)v2 = input - 1;

		++area[u1 + v1*input];
		if (u1 != u2)++area[u2 + v1*input];
		if (v1 != v2)++area[u1 + v2*input];
		if (u1 != u2 && v1 != v2)++area[u2 + v2*input];
	}
	

	for (int i = 0; i < input*input; ++i) {
		if (area[i] == 3) {
			face[i].push_back(face_buffer);
			for(int j=0;j<n;++j)
				vt_area.push_back(i);
			break;
		}
		else
			++f_count;
	}
	if (f_count == input*input)
		cout << "error!!";
	for (int i = 0; i < input*input; ++i)
		area[i] = 0;
	vector<string>().swap(face_buffer);
}
int main() {
	int start = 0;
	int mtl_count = 0;
	int input = 2;
	int ref_count=0;
	cout << "分割回数を入力してください :";
	cin >> input;
	double uv_value;
	double div;
	div=(1 / double(input));
	
	string str;
	string mtl_name;
	vector<int> vt_area;
	vector<double> u;
	vector<double> v;
	vector<double> u_fix;
	vector<double> v_fix;
	vector<vector<string>> *face = new vector<vector<string>>[input*input];

	//入力した名前のファイルを開く
	char nm_fin[100], nm_fout[100];
	cout << "入力ファイル名を入力してください :";
	cin >> nm_fin;
	ifstream fin;
	fin.open(nm_fin);
	if (fin.fail()) {
		cout << "fin.open() failed." << endl;
		return 9;
	}

	//入力した名前のファイルを出力する
	cout << "出力ファイル名を入力してください :";
	cin >> nm_fout;
	ofstream fout;
	fout.open(nm_fout);
	if (fout.fail()) {
		cout << "fout.open() failed." << endl;
		return 9;
	}

	//読み込みと出力
	while (true) {
		fin >> str;
		if (fin.fail()) break;
		if (str[0] == '#') {
			if (start == 0) {
				fout << str;
				start = 1;
			}
			else if (start == 1)
				fout << endl << str;
		}

		else if (str == "mtllib" || str == "s" || str == "o" || str == "v" || str == "vn")
			fout << endl << str;
		//テクスチャ座標値を配列に保存
		else if (str == "vt") {
			fin >> str;
			uv_value = stod(str);
			if (uv_value > 1)uv_value = 1;
			if (uv_value < 0)uv_value = 0;
			u.push_back(uv_value);
			fin >> str;
			uv_value = stod(str);
			if (uv_value > 1)uv_value = 1;
			if (uv_value < 0)uv_value = 0;
			v.push_back(uv_value);
		}

		else if (str == "usemtl") {
			//material名の読み込みが１回目のとき
			if (mtl_count == 0) {
				fin >> mtl_name;
				++mtl_count;
			}
			//material名の読み込みが二回目以降のとき
			else if (mtl_count > 0) {
				if (mtl_count == 1) {
					vtConverter(fout, u, v, div, input);
					
				}
				vtConverter2(fout, u_fix, v_fix, div, input, vt_area);
				faceConverter(fout, face, mtl_name, input);
				fin >> mtl_name;
				vector<double>().swap(u_fix);
				vector<double>().swap(v_fix);
				for (int i = 0; i < input*input; ++i)
					vector<vector<string>>().swap(face[i]);
				++mtl_count;
			}
		}

		else if (str == "f") {
			faceClassification(fout, fin, u, v, u_fix,v_fix, face, input,vt_area,div,ref_count);
		}

		else
			fout << " " << str;
	}
	//テクスチャ座標の値を修正して書き込む
	if (mtl_count == 1) {
		vtConverter(fout, u, v, div, input);
	}
	vtConverter2(fout, u_fix, v_fix, div, input, vt_area);
	faceConverter(fout, face, mtl_name, input);
	vector<double>().swap(u_fix);
	vector<double>().swap(v_fix);
	fin.close();
	fout.close();
}