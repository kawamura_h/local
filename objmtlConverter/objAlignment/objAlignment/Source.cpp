#include <iostream>
#include <fstream>
#include <string>
#include <vector>

using namespace std;


int main() {
	int start = 0;
	string str;
	vector<double> vt1;
	vector<double> vt2;
	vector<string> mf;

	//入力した名前のファイルを開く
	char nm_fin[100], nm_fout[100];
	cout << "入力ファイル名を入力してください :";
	cin >> nm_fin;
	ifstream fin;
	fin.open(nm_fin);
	if (fin.fail()) {
		cout << "fin.open() failed." << endl;
		return 9;
	}

	//入力した名前のファイルを出力する
	cout << "出力ファイル名を入力してください :";
	cin >> nm_fout;
	ofstream fout;
	fout.open(nm_fout);
	if (fout.fail()) {
		cout << "fout.open() failed." << endl;
		return 9;
	}

	//読み込みと出力
	while (true) {
		fin >> str;
		if (fin.fail()) break;

		if (str[0] == '#') {
			if (start == 0) {
				fout << str;
				start = 1;
			}
			else if (start == 1)
				fout << endl << str;
		}

		else if (str == "mtllib" || str == "s" || str == "o" || str == "v" || str == "vn")
			fout << endl << str;

		else if (str == "vt") {
			//テクスチャ座標値を配列に保存
			fin >> str;
			vt1.push_back(stod(str));
			fin >> str;
			vt2.push_back(stod(str));
		}

		else if (str == "usemtl") {
			mf.push_back(str);
			fin >> str;
			mf.push_back(str);
		}

		else if (str == "f") {
			mf.push_back(str);
			fin >> str;
			mf.push_back(str);
			fin >> str;
			mf.push_back(str);
			fin >> str;
			mf.push_back(str);
		}

		else
			fout << " " << str;
	}
	//vtの書き込み
	for (int i = 0; i < int(vt1.size()); ++i)
		fout << endl << "vt " << vt1[i] << ' ' << vt2[i];
	//faceの書き込み
	for (int i = 0; i<int(mf.size()); ++i) {
		if (mf[i] == "usemtl" || mf[i] == "f") {
			fout << endl << mf[i];
		}
		else {
			fout << ' ' << mf[i];
		}
	}

	fin.close();
	fout.close();
}