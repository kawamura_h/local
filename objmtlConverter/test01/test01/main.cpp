#include <iostream>//cout,cin
#include <fstream>//テキストファイル入出力
#include <string>
#include <vector>

using namespace std;

int main() {
	vector<double> vt1;
	vector<double> vt2;

	vector<string> face;
	vector<vector<string>> f1;
	vector<vector<string>> f2;
	vector<vector<string>> f3;
	vector<vector<string>> f4;

	string str;
	string uvs;
	string mtl_name;
	int start=0;
	int vt_start, vt_end;
	int vt_count = 0;
	int i, k;
	int j = 0;
	int area_a = 0, area_b = 0, area_c = 0, area_d = 0;
	int mtl_count = 0;
	int input;
	double rate_1 = 0.55;
	double rate_2 = 1 - rate_1;
	double ud[3];
	double vd[3];


	//入力した名前のファイルを開く
	char nm_fin[100], nm_fout[100];
	cout << "Input File name = ";
	cin >> nm_fin;
	ifstream fin;
	fin.open(nm_fin);
	if (fin.fail()) {
		cout << "fin.open() failed." << endl;
		return 9;
	}

	//入力した名前のファイルを出力する
	cout << "output file name = ";
	cin >> nm_fout;
	ofstream fout;
	fout.open(nm_fout);
	if (fout.fail()) {
		cout << "fout.open() failed." << endl;
		return 9;
	}

	//読み込みと出力
	while (true) {
		fin >> str;
		if (fin.fail()) break;

		if (str == "#") {
			if (start == 0) {
				fout << str;
				start = 1;
			}
			else if (start == 1)
				fout << endl << str;
		}
		else if (str == "mtllib" || str == "s" || str == "o" || str == "v" || str == "vn")
			fout << endl << str;
		
		else if (str == "vt") {
			//テクスチャ座標値を配列に保存
			fin >> str;
			vt1.push_back(stod(str));
			fin >> str;
			vt2.push_back(stod(str));
		}
		else if (str == "usemtl") {
			//material名の読み込みが１回目のとき
			if (mtl_count == 0) {
				fin >> mtl_name;
				++mtl_count;
			}
			//material名の読み込みが二回目以降のとき
			else if (mtl_count > 0) {
				//テクスチャ座標値の修正
				for (i = 0; i<int(vt1.size());i++) {
					if (vt1[i] <= rate_1 && vt2[i] <= rate_1) {
						fout << endl << "vt " << (vt1[i]*(1/rate_1)) << " " << (vt2[i] * (1 / rate_1));
					}
					else if (vt1[i] > rate_2 && vt2[i] <= rate_1) {
						fout << endl << "vt " << ((vt1[i]-rate_2) * (1 / rate_1)) << " " << (vt2[i] * (1 / rate_1));
					}
					else if (vt1[i] <= rate_1 && vt2[i] > rate_2) {
						fout << endl << "vt " << (vt1[i] * (1 / rate_1)) << " " << ((vt2[i]-rate_2) * (1 / rate_1));
					}
					else if (vt1[i] > rate_2 && vt2[i] > rate_2) {
						fout << endl << "vt " << ((vt1[i]-rate_2) * (1 / rate_1)) << " " << ((vt2[i]-rate_2) * (1 / rate_1));
					}
				}

				//振り分けたfaceを並び替え，領域に対応したマテリアルを割り当てる
				fout << endl << "usemtl a_" << mtl_name;
				for (i = 0; i < int(f1.size()); ++i) {
					fout << endl << "f";
					for (j = 0; j < 3; ++j) {
						fout << " " << f1[i][j];
					}
				}
				fout << endl << "usemtl b_" << mtl_name;
				for (i = 0; i < int(f2.size()); ++i) {
					fout << endl << "f";
					for (j = 0; j < 3; ++j) {
						fout << " " << f2[i][j];
					}
				}
				fout << endl << "usemtl c_" << mtl_name;
				for (i = 0; i < int(f3.size()); ++i) {
					fout << endl << "f";
					for (j = 0; j < 3; ++j) {
						fout << " " << f3[i][j];
					}
				}
				fout << endl << "usemtl d_" << mtl_name;
				for (i = 0; i < int(f4.size()); ++i) {
					fout << endl << "f";
					for (j = 0; j < 3; ++j) {
						fout << " " << f4[i][j];
					}
				}

				
				fin >> mtl_name;
				++mtl_count;
			}
		}

		else if (str == "f") {
			//faceの情報の読み込み
			fin >> str;
			face.push_back(str);
			fin >> str;
			face.push_back(str);
			fin >> str;
			face.push_back(str);
			
			
			//faceが参照するテクスチャ座標ベクトルの特定
			for (i = 0; i < 3; ++i) {
				while (vt_count < 2) {
					if (face[i][j] == '/' && vt_count == 0) {
						vt_start = j;
						++vt_count;
						++j;
					}
					else if (face[i][j] == '/' && vt_count == 1) {
						vt_end = j;
						++vt_count;
						++j;
					}
					else {
						++j;
					}
				}
				vt_count = 0;
				j = 0;
				for (k = vt_start + 1; k < vt_end; ++k) {
					uvs.push_back(face[i][k]);
				}
				//元々のテクスチャ座標値を取得
				ud[i] = vt1[stoi(uvs) - 1];
				vd[i] = vt2[stoi(uvs) - 1];
				for (k = vt_start+1; k < vt_end; ++k) {
					uvs.pop_back();
				}
			}
			//テクスチャ座標値による場合分け
			//faceの頂点が全て同じ領域にある場合，faceをその領域に追加
			for (i = 0; i < 3; i++) {
				if (ud[i] <= rate_1 && vd[i] < rate_1) {
					++area_a;
				}
				if (ud[i] > rate_2 && vd[i] <= rate_1) {
					++area_b;
				}
				if (ud[i] <= rate_1 && vd[i] > rate_2) {
					++area_c;
				}
				if (ud[i] > rate_2 && vd[i] > rate_2) {
					++area_d;
				}
			}
			if (area_a == 3)
				f1.push_back(face);
			else if (area_b == 3) 
				f2.push_back(face);
			else if (area_c == 3) 
				f3.push_back(face);
			else if (area_d == 3) 
				f4.push_back(face);
			else if (area_a < 3 && area_b < 3 && area_c < 3 && area_d < 3) {
				//エラー時の処理
				cout << "error"<< endl;
			}
			area_a = 0;
			area_b = 0;
			area_c = 0;
			area_d = 0;

			face.pop_back();
			face.pop_back();
			face.pop_back();
		}
		else
			fout << " " << str;
	}
	//もとのmaterialが1種類の場合
	if (mtl_count==1) {
		//テクスチャ座標値を修正
		for (i = 0; i<int(vt1.size()); i++) {
			if (vt1[i] <= rate_1 && vt2[i] <= rate_1) {
				fout << endl << "vt " << (vt1[i] * (1 / rate_1)) << " " << (vt2[i] * (1 / rate_1));
			}
			else if (vt1[i] > rate_2 && vt2[i] <= rate_1) {
				fout << endl << "vt " << ((vt1[i] - rate_2) * (1 / rate_1)) << " " << (vt2[i] * (1 / rate_1));
			}
			else if (vt1[i] <= rate_1 && vt2[i] > rate_2) {
				fout << endl << "vt " << (vt1[i] * (1 / rate_1)) << " " << ((vt2[i] - rate_2) * (1 / rate_1));
			}
			else if (vt1[i] > rate_2 && vt2[i] > rate_2) {
				fout << endl << "vt " << ((vt1[i] - rate_2) * (1 / rate_1)) << " " << ((vt2[i] - rate_2) * (1 / rate_1));
			}
		}

		//振り分けたfaceを並び替え，領域に対応したマテリアルを割り当てる
		fout << endl << "usemtl a_" << mtl_name;
		for (i = 0; i < int(f1.size()); ++i) {
			fout << endl << "f";
			for (j = 0; j < 3; ++j) {
				fout << " " << f1[i][j];
			}
		}
		fout << endl << "usemtl b_" << mtl_name;
		for (i = 0; i < int(f2.size()); ++i) {
			fout << endl << "f";
			for (j = 0; j < 3; ++j) {
				fout << " " << f2[i][j];
			}
		}
		fout << endl << "usemtl c_" << mtl_name;
		for (i = 0; i < int(f3.size()); ++i) {
			fout << endl << "f";
			for (j = 0; j < 3; ++j) {
				fout << " " << f3[i][j];
			}
		}
		fout << endl << "usemtl d_" << mtl_name;
		for (i = 0; i < int(f4.size()); ++i) {
			fout << endl << "f";
			for (j = 0; j < 3; ++j) {
				fout << " " << f4[i][j];
			}
		}
	}
	
	fin.close();
	fout.close();
}

