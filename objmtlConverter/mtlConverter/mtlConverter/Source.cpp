#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
	int input;
	cout << "分割回数を入力してください :";
	cin >> input;
	int start = 0;
	int mtl_size;
	string str;
	vector<string> mtl_load;
	
	
	char nm_fin[100], nm_fout[100];
	cout << "Input File name = ";
	cin >> nm_fin;
	ifstream fin;
	fin.open(nm_fin);
	if (fin.fail()) {
		cout << "fin.open() failed." << endl;
		return 9;
	}
	
	cout << "output file name = ";
	cin >> nm_fout;
	ofstream fout;
	fout.open(nm_fout);
	if (fout.fail()) {
		cout << "fout.open() failed." << endl;
		return 9;
	}

	while (true) {
		fin >> str;
		if (fin.fail())break;
		
		if (str[0] == '#') {
			if (start == 0) {
				fout << str;
				start = 1;
			}
			else if (start == 1)
				fout << endl << str;
		}
		//materialの読み込み
		else if (str == "newmtl") {
			while (true) {
				if (str=="map_Kd"){
					mtl_load.push_back(str);
					fin >> str;
					mtl_load.push_back(str);
					break;
				}
				else {
					mtl_load.push_back(str);
					fin >> str;
				}
			}
			//materialを4つに分割
			for (int i=0; i < input*input; ++i) {
				for (int j = 0; j < mtl_load.size(); ++j) {
					//改行が必要な場合
					if (mtl_load[j] == "Ns" || mtl_load[j] == "Ka" || mtl_load[j] == "Kd" || mtl_load[j] == "Ks" ||
						mtl_load[j] == "Ke" || mtl_load[j] == "Ni" || mtl_load[j] == "d" || mtl_load[j] == "illum" || mtl_load[j] == "Tr") {
						fout << endl << mtl_load[j];
					}
					//material名に番号を追加
					else if (mtl_load[j] == "newmtl") {
						fout << endl << mtl_load[j];
						++j;
						fout << " " << to_string(i) << "_" << mtl_load[j];
					}
					//参照するjpg画像の名前にa〜dを追加
					else if (mtl_load[j] == "map_Kd") {
						fout << endl << mtl_load[j];
						++j;
						fout << " " << to_string(i) << "_" << mtl_load[j]<<endl;
					}
					else {
						fout << " " << mtl_load[j];
					}
				}
			}
			vector<string>().swap(mtl_load);
		}
		else
			fout << " " << str;
	}
	fin.close();
	fout.close();
}