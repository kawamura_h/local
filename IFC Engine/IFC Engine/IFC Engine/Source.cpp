#include <stdio.h>
#include <stdlib.h>
#include "engine.h"
#include "ifcengine.h"
#include <iostream>
#include <fstream>

using namespace std;

int main() {
	int_t *ifcObjectInstances, ifcObjectInstance;
	int64_t ifcModel = sdaiOpenModelBN(0, "WakamiyaBridge.ifc","IFC4_ADD1_BMaintenance.exp");

	if (ifcModel) {

		ifcObjectInstances = sdaiGetEntityExtentBN(ifcModel, "IFCBUILDINGELEMENTPROXY");
		engiGetAggrElement(ifcObjectInstances, 0, sdaiINSTANCE, &ifcObjectInstance);

		if (ifcObjectInstance) {

			int_t setting = 0, mask = 0;
			mask += 0;
			mask += 0;
			mask += 32;
			mask += 256;
			mask += 0;
			mask += 0;
			mask += 0;

			setting += 0;
			setting += 0;
			setting += 32;
			setting += 256;
			setting += 0;
			setting += 0;
			setting += 0;
			setFormat(ifcModel, setting, mask);
			circleSegments(32, 5);

			__int64 noVertices = 0, noIndices = 0;
			CalculateInstance(ifcObjectInstance, &noVertices, &noIndices, nullptr);

			float *vertices = new float[(int)noVertices * 6];
			int32_t* indices = new int32_t[(int)noIndices];
			UpdateInstanceVertexBuffer(ifcObjectInstance, vertices);
			UpdateInstanceIndexBuffer(ifcObjectInstance, indices);

			int_t startIndexTriangles = 0, noIndicesTriangles = 0;
			int_t faceCnt = getConceptualFaceCnt(ifcObjectInstance);

			for (int i = 0; i < faceCnt; i++) {
				getConceptualFaceEx(ifcObjectInstance, i, &startIndexTriangles, &noIndicesTriangles, 0, 0, 0, 0, 0, 0, 0, 0);
			}

			for (int i = 0; i < sizeof(vertices); i++) {
				cout << vertices[i] << endl;
			}
			for (int i = 0; i < sizeof(indices); i++) {
				cout << indices[i] << endl;
			}
		}
	}
	else
		cout << "error"<<endl;

}
